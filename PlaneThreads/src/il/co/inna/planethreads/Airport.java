package il.co.inna.planethreads;

public class Airport {
	private Object runway_lock;
	public Airport()
	{
		runway_lock = new Object();
	}
	Object getRunway(Object a){
		System.out.println("Airport: " + a.toString()+ " wants to monopolize the runway.");
		return runway_lock;
	}
}
