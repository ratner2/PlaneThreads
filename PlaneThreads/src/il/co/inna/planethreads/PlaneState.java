package il.co.inna.planethreads;

public enum PlaneState {	
	    GROUND, SKY, LANDING, TAKEOFF 
}
