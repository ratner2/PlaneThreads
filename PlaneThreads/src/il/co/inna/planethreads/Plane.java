package il.co.inna.planethreads;




public class Plane implements Runnable {	
	
	private static int plane_counter = 0;	
	private Airport airport;
	private PlaneState state;
	private int id = 0;
	public Plane(Airport a)
	{
		airport = a;
		state = PlaneState.GROUND;
		id = 1000 + Plane.plane_counter++;
	}
	public static int getPlaneCounter()
	{
		return plane_counter;
	}
	@Override
	public void run() {
		this.TakeOff();
		this.Fly();
		this.Land();		
	}
	private void Land() {
		if(state != PlaneState.SKY && state != PlaneState.LANDING)
		{
			System.out.println("BadPlane! Plane "+id+" trying to land while on the ground.");
			return;
		}
		System.out.println("Plane "+id+" wants to land.");
		synchronized(airport.getRunway(this))
		{			
			state = PlaneState.GROUND;
			System.out.println("Plane "+id+" landed.");
		}
	}
	private void Fly() {
		long millis = 1000 + Math.round(Math.random()*5000);
		System.out.println("Plane "+id+" flying for "+millis+"ms");
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			System.out.println("Plane "+id+" was interrupted while flying!");
			return;
		}
		
	}
	private void TakeOff() {
		if(state != PlaneState.GROUND && state != PlaneState.TAKEOFF)
		{
			System.out.println("BadPlane! Plane "+id+" trying to takeoff while in the sky.");
			return;
		}
		System.out.println("Plane "+id+" wants to take off.");
		synchronized(airport.getRunway(this))
		{			
			state = PlaneState.SKY;
			System.out.println("Plane "+id+" took off.");
		}
		
	}
	@Override
	public String toString()
	{
		return "plane_object["+id+"]";
	}
}
