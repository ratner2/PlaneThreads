package il.co.inna.planethreads;

public class AirportManager {
	private final static int NUM_OF_PLANES = 30;
	
	public static void main(String[] args) {
		Airport a = new Airport();
		Plane[] allplanes = new Plane[NUM_OF_PLANES];
		Thread[] threads = new Thread[NUM_OF_PLANES];
		for(int i = 0; i < allplanes.length; i++)
		{
			allplanes[i] = new Plane(a);
		}
		
		//Fly babies! Fly!
		for(int i = 0; i < allplanes.length; i++)
		{
			threads[i] = new Thread(allplanes[i]);
		}		
		for(Thread t: threads)
			t.start();
		System.out.println("MainSystem:  done starting threads.");
		
		for(int i = 0; i < allplanes.length; i++)
		{			
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				System.out.print("Problem join() on thread "+i);				
			}
		}	
		System.out.print("All of " + allplanes.length + " landed. Closing the Airport.");
	}

}
